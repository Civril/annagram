package com.pl.code.kata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

public class AnnagramUtils {

	/**
	 * Sort string in alphabetical order
	 * @param word
	 * @return
	 */
	public String sortString(String word) {
		// put the characters into an array
		Character[] chars = new Character[word.length()];
		for (int i = 0; i < chars.length; i++)
			chars[i] = word.charAt(i);

		// sort the array
		Arrays.sort(chars, new Comparator<Character>() {
			public int compare(Character c1, Character c2) {
				int cmp = Character.compare(Character.toLowerCase(c1.charValue()),
						Character.toLowerCase(c2.charValue()));
				if (cmp != 0)
					return cmp;
				return Character.compare(c1.charValue(), c2.charValue());
			}
		});

		// rebuild the string
		String returnString;
		StringBuilder sb = new StringBuilder(chars.length);
		for (char c : chars)
			sb.append(c);
		returnString = sb.toString();
		return returnString;
	}

	/**
	 * Reads all annagrams from file, and puts in on hashmap
	 * @param fileName
	 * @return
	 */
	public Map<String, ArrayList<Long>> readAllAnnagramsFromFile(String fileName) {
		System.out.println("Reading file & looking for annagrams, please wait...");
		long lineNumber = 0;
		Map<String, ArrayList<Long>> annagramList = new HashMap<String, ArrayList<Long>>();

		Charset charset = Charset.forName("US-ASCII");
		File file = new File(fileName);
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				lineNumber++;
				int lenght = line.length();
				String sortWord = sortString(line.replace("'", "").toLowerCase());
				if (annagramList.containsKey(sortWord)) {
					annagramList.get(sortWord).add(lineNumber);
				} else {
					ArrayList<Long> listLines = new ArrayList<Long>();
					listLines.add(lineNumber);
					annagramList.put(sortWord, listLines);
				}
			}
		} catch (IOException x) {
			System.err.format("IOException: %s%n", x);
		}

		Iterator<Map.Entry<String, ArrayList<Long>>> iter = annagramList.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, ArrayList<Long>> entry = iter.next();
			if (entry.getValue().size() <= 1) {
				iter.remove();
			}
		}
		lineNumber = 0;
		return annagramList;
	}

	
	/**
	 * Read specific line from given file
	 * @param wordLineNumber
	 * @param fileName
	 * @return
	 */
	public String getSpecificLineFromFile(Long wordLineNumber, String fileName) {
		String line = "";
		try (Stream<String> lines = Files.lines(Paths.get(fileName), Charset.defaultCharset())) {
			line = lines.skip(wordLineNumber - 1).findFirst().get();
		} catch (IOException e) {
			System.out.println("Read file somethink went wrong:");
			e.printStackTrace();
		}
		return line;
	}

	/**
	 * Finds longest annagrams on hashmap
	 * @param annagramList
	 * @param fileName
	 * @return
	 */
	public List<String> findLongestAnnagram(Map<String, ArrayList<Long>> annagramList, String fileName) {
		int maxLength = 0;
		String maxLengthKey = "";
		for (Map.Entry<String, ArrayList<Long>> entry : annagramList.entrySet()) {
			String key = entry.getKey();
			if (key.length() > maxLength) {
				maxLength = key.length();
				maxLengthKey = key;
			}
		}

		return getAnnagramsByKey(maxLengthKey, annagramList, fileName);
	}

	/**
	 * Finds longest set of annagrams
	 * @param annagramList
	 * @param fileName
	 * @return
	 */
	public List<String> findLongestSetOfAnnagrams(Map<String, ArrayList<Long>> annagramList, String fileName) {
		int maxSetAnnagrams = 0;
		String maxSethKey = "";
		for (Map.Entry<String, ArrayList<Long>> entry : annagramList.entrySet()) {
			String key = entry.getKey();
			ArrayList<Long> value = entry.getValue();
			if (value.size() > maxSetAnnagrams) {
				maxSetAnnagrams = value.size();
				maxSethKey = key;
			}
		}

		return getAnnagramsByKey(maxSethKey, annagramList, fileName);
	}

	/**
	 * Returns all annagrams to given key (chars in alphabetical order)
	 * @param key
	 * @param annagramList
	 * @param fileName
	 * @return
	 */
	public List<String> getAnnagramsByKey(String key, Map<String, ArrayList<Long>> annagramList, String fileName) {
		ArrayList<Long> annagramsLines = annagramList.get(key);
		List<String> annagramsSet = new ArrayList<String>();
		for (Long lineNumber : annagramsLines) {
			String annagram = getSpecificLineFromFile(lineNumber, fileName);
			annagramsSet.add(annagram);
		}
		return annagramsSet;
	}
}
