package com.pl.code.kata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Annagram {

	public void run(String filePath) {
		

		AnnagramUtils stringUtils = new AnnagramUtils();
		Map<String, ArrayList<Long>> annagramList = stringUtils.readAllAnnagramsFromFile(filePath);

		System.out.println("Number of annagrams: " + annagramList.size());
		List<String> longestAnnagram = stringUtils.findLongestAnnagram(annagramList, filePath);
		System.out.println("Longest annagrams: ");
		for (String word : longestAnnagram) {
			System.out.print(word + ", ");
		}
		List<String> longestSetOfAnnagram = stringUtils.findLongestSetOfAnnagrams(annagramList, filePath);
		System.out.println("");
		System.out.println("Longest set of annagrams: ");
		for (String word : longestSetOfAnnagram) {
			System.out.print(word + ", ");
		}
	}

}
