package com.pl.code.kata;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class app {

	public static void main(String[] args) {
		if (args.length == 0) {
			throw new IllegalArgumentException("No directory given to index");
		}
		String directory = args[0];
		Annagram annagram = new Annagram();
		annagram.run(directory);
	}

}
