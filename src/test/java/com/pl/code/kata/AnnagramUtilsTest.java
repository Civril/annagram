package com.pl.code.kata;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.pl.code.kata.AnnagramUtils;

public class AnnagramUtilsTest {

	AnnagramUtils stringUtils = new AnnagramUtils();
	static String  absolutePath;

	@BeforeClass
	public static void beforeClass() {
		// define absolute path to test file
		File file = new File("src//test//resources//wordlist_test.txt");
		absolutePath = file.getAbsolutePath();
		
	}

	@Test
	public void testSortString() {
		String string1 = "cba";
		String string2 = "zrtwade";

		String sortedString1 = stringUtils.sortString(string1);
		String sortedString2 = stringUtils.sortString(string2);

		assertEquals("abc", sortedString1);
		assertEquals("adertwz", sortedString2);
	}

	@Test
	public void testReadAllAnnagramsFromFile() {
		String fileName = "";
		Map<String, ArrayList<Long>> annagramsMapExpected = new HashMap<String, ArrayList<Long>>();
		annagramsMapExpected.put("act", new ArrayList<Long>(Arrays.asList((long) 1, (long) 2, (long) 3)));
		annagramsMapExpected.put("eips", new ArrayList<Long>(Arrays.asList((long) 5, (long) 9)));
		annagramsMapExpected.put("adilpsy", new ArrayList<Long>(Arrays.asList((long) 7, (long) 8)));

		Map<String, ArrayList<Long>> annagramsMapResult = stringUtils.readAllAnnagramsFromFile(absolutePath);

		// test equal
		assertThat(annagramsMapResult, is(annagramsMapExpected));
		// Test size
		assertThat(annagramsMapResult.size(), is(3));
		// Test map entry
		assertThat(annagramsMapResult,
				IsMapContaining.hasEntry("act", new ArrayList<Long>(Arrays.asList((long) 1, (long) 2, (long) 3))));
		assertThat(annagramsMapResult,
				IsMapContaining.hasEntry("eips", new ArrayList<Long>(Arrays.asList((long) 5, (long) 9))));
		assertThat(annagramsMapResult,
				IsMapContaining.hasEntry("adilpsy", new ArrayList<Long>(Arrays.asList((long) 7, (long) 8))));
	}

	@Test
	public void testGetSpecificLineFromFile() {
		String expectedLine = "dog";

		String resultLine = stringUtils.getSpecificLineFromFile((long) 4, absolutePath);

		assertEquals(expectedLine, resultLine);
	}

	@Test
	public void testFindLongestAnnagram() {
		List<String> expectedList = new ArrayList<String>(Arrays.asList("display", "dispyal"));
		Map<String, ArrayList<Long>> annagramsMapResult = stringUtils.readAllAnnagramsFromFile(absolutePath);

		List<String> resultList = stringUtils.findLongestAnnagram(annagramsMapResult, absolutePath);

		assertThat(resultList, is(expectedList));
	}

	@Test
	public void testfindLongestSetOfAnnagrams() {
		List<String> expectedList = new ArrayList<String>(Arrays.asList("cat", "tac", "cta"));
		Map<String, ArrayList<Long>> annagramsMapResult = stringUtils.readAllAnnagramsFromFile(absolutePath);

		List<String> resultList = stringUtils.findLongestSetOfAnnagrams(annagramsMapResult, absolutePath);

		assertThat(resultList, is(expectedList));
	}
	
	@Test
	public void testGetAnnagramsByKey() {
		List<String> expectedList = new ArrayList<String>(Arrays.asList("cat", "tac", "cta"));
		Map<String, ArrayList<Long>> annagramsMapResult = stringUtils.readAllAnnagramsFromFile(absolutePath);
		
		List<String> resultList = stringUtils.getAnnagramsByKey("act", annagramsMapResult, absolutePath);
		
		assertThat(resultList, is(expectedList));
	}
}
